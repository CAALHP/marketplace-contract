﻿namespace MarketplaceContract
{
    public class UserProfile : User
    {

        public string FristName { get; set; }
        public string LastName { get; set; }
        public string ProfilePhotoURL { get; set; }

    }
}