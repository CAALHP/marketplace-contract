﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketplaceContract
{
    public class ApplicationProfile:Application
    {
        public string Description { get; set; }
        public String[] ImagesUrls { get; set; }

    }
}
