﻿namespace MarketplaceContract
{
    public class User
    {
        public string UserId { get; set; }
        public UserRoleType UserRole { get; set; }
        public string[] Templates { get; set; }
        public string NFC { get; set; }
    }

    public enum UserRoleType
    {
        
        Citizen,
        Caregiver,
        Vendor,
        Manager,
        Admin
    }


}
