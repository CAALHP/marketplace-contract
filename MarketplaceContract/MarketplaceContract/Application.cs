﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketplaceContract
{
    public class Application
    {

        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }


    }
}
